# digit-value-field

### How to compile

```bash
cd testone-interview/digit-value-field
npm i
npm run serve
```




## Purpose

The purpose of this question is to create a custom component that can display values ​​in it and change the value digit by digit (increase or decrease).


## Instructions
 You can find the component in  *'src/components/valueArea'*
 (Note: no need to add extra classes for styling)

 - When the element is clicked, the part of the value in the element before the decimal point will be blinked. (you can use *'.selected'* class to do this blinking)
 -  Each time the **space key** is pressed while the element is active (blinking), the active digit will be shifted to the right.
 -  When it (blinking active digit) reaches the last digit it will go back to the beginning.
 -  If the **arrow up key** is pressed on the active digit, the active digit value will increase by one,
 -  If the **arrow down key** is pressed on the active digit, the active digit value will decrease by one,
- Style of the element will be reset when the element loses focus. (Everything will be as it was originally (no blink, no background color))


## Like that:

![Alt Text](digit.gif)

