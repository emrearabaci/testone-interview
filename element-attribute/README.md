# Custon Element Attribute

### How to compile

```bash
cd testone-interview/element-attribute
npm i
npm run serve
```



## Purpose

We wanted to control all input's style and value's via custom element attribute.

## Instructions

 - Add a custom attribute (use directives) named as 'color' then  assign **'bgColor'** variable to the attribute  as reference value
 -  'bgColor' can be changed by the buttons at the bottom.
 -  Elements will be updated when color attribute ref value (bgColor) changes.
 -  Also, Input values should be *'bgColor + ElementName'* (e.g. pink Element1, red Element3).
 -  Active color button should be disable until select another color button.

**Note: Default background color have to be red and input values empty. (don't use extra classes, everything have to be controlled via attribute (color directive) )**
  



## Like that:

![Alt Text](attribute.gif)

