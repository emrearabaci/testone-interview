# Observe Store Value

### How to compile

```bash
cd testone-interview/observe-store-value
npm i
npm run serve
```

## Purpose

We wanted to observe the changes test settings in store values and show these current values and old values on the table. 

## Instructions

Bind input values from the store.

```javascript

/src/store/index.js

export default new Vuex.Store({
  state: {
    testSettings: {
      phaseValues: {
        phase: 0,
      },
      frequencyValues: {
        frequency: 0,
      },
    },
  },
});

```


 - Model value of Phase input should be **phase** value in store.
 - Model value of Frequency input should be **frequency** value in store.
 - The values ​​in the store should be updated for every change in the inputs (synchronously)
 - Current values and old values should be able to observed in related sections of the table.

## Like that:

![Alt Text](observe.gif)

