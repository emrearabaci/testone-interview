import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    testSettings: {
      phaseValues: {
        phase: 0
      },
      frequencyValues: {
        frequency: 0
      }
    }
  },
  mutations: {
    setNewValues (state, newValues) {
      state.testSettings = newValues
    }
  },
  actions: {
    async setNewValues (context, data) {
      await context.commit('setNewValues', data)
    }
  }
})
